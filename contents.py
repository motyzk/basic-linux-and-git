import pathlib
contents = open("contents.txt", "a+")


def copy_text(folder):
    global contents
    for file_or_dir in folder.iterdir():
        if (not file_or_dir.is_dir()) and ".txt" in str(file_or_dir):
            with open(str(file_or_dir)) as f:
                for line in f:
                    contents.write(line)
            contents.write("\n")
        elif file_or_dir.is_dir():
            copy_text(file_or_dir)


first_folder = pathlib.Path('.')
copy_text(first_folder)

